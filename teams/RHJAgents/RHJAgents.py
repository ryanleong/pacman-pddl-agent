# RHJAgents.py
# Adapts code from ffAgents
# -----------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html
# This code have been adapted by Nir Lipvetzky and Toby Davies at Unimelb to run FF planner

from captureAgents import CaptureAgent
from captureAgents import AgentFactory
import distanceCalculator
import random, time, util
from game import Directions
import keyboardAgents
import game
from util import nearestPoint
import os

#############
# FACTORIES #
#############

NUM_KEYBOARD_AGENTS = 0
class RHJAgents(AgentFactory):
  "Returns one keyboard agent and offensive reflex agents"

  def __init__(self, isRed, first='offense', second='defense', rest='offense'):
    AgentFactory.__init__(self, isRed)
    self.agents = [first, second]
    self.rest = rest

  def getAgent(self, index):
    if len(self.agents) > 0:
      return self.choose(self.agents.pop(0), index)
    else:
      return self.choose(self.rest, index)

  def choose(self, agentStr, index):
    if agentStr == 'keys':
      global NUM_KEYBOARD_AGENTS
      NUM_KEYBOARD_AGENTS += 1
      if NUM_KEYBOARD_AGENTS == 1:
        return keyboardAgents.KeyboardAgent(index)
      elif NUM_KEYBOARD_AGENTS == 2:
        return keyboardAgents.KeyboardAgent2(index)
      else:
        raise Exception('Max of two keyboard agents supported')
    elif agentStr == 'offense':
      return OffensiveReflexAgent(index)
    elif agentStr == 'defense':
      return DefensiveReflexAgent(index)
    elif agentStr == 'RHJAgents':
      return ReflexCaptureAgent(index)
    else:
      raise Exception("No staff agent identified by " + agentStr)

class AllOffenseAgents(AgentFactory):
  "Returns one keyboard agent and offensive reflex agents"

  def __init__(self, **args):
    AgentFactory.__init__(self, **args)

  def getAgent(self, index):
    return OffensiveReflexAgent(index)

class OffenseDefenseAgents(AgentFactory):
  "Returns one keyboard agent and offensive reflex agents"

  def __init__(self, **args):
    AgentFactory.__init__(self, **args)
    self.offense = False

  def getAgent(self, index):
    self.offense = not self.offense
    if self.offense:
      return OffensiveReflexAgent(index)
    else:
      return DefensiveReflexAgent(index)

##########
# Agents #
##########

class ReflexCaptureAgent(CaptureAgent):
  def __init__( self, index, timeForComputing = .1 ):
    CaptureAgent.__init__( self, index, timeForComputing)
    self.visibleAgents = []

  def createPDDLobjects(self):
    """
    Return a list of objects describing grid positions of the layout
    This list is used in the PDDL problem file 
    """    

    # Listing all reachable positions (those not walls)
    
    obs = self.getCurrentObservation()
    grid = obs.getWalls()
    locations = grid.asList(False);

    result = '';
    for (x,y) in locations:
      result += 'p_%d_%d '%(x,y)

    result += '- location'

    return result

  def createPDDLfluents(self):
    """
    Return a list of fluents describing the state space This list is
    used in the PDDL problem file

    To describe the Initial State of the PDDL problem file
    """
    
    result = ''
    obs = self.getCurrentObservation()

    #
    # Model myself (the current agent)
    #
    (myx,myy) = obs.getAgentPosition( self.index )

    result += '(at p_%d_%d) ' % (myx,myy)

    if obs.getAgentState( self.index ).isPacman is True:
      result += '(is_pacman) '
    else:
      result += '(is_ghost) '
    
    #
    # Model opponent team if visible and store them in an array
    #
    enemyPos = []
    """self.visibleAgents = []"""
    other_team = self.getOpponents( obs )
    distances = obs.getAgentDistances()
    for i in other_team:
      
      if obs.getAgentPosition(i) is None: 
        continue
      
      (x,y) = obs.getAgentPosition(i)
      x = int(x)
      y = int(y)
      if obs.getAgentState(i).isPacman is False:
        result += '(enemy_ghost_at p_%d_%d) '%(x,y)
        enemyPos.append((x,y))
      else:
        result += '(enemy_pacman_at p_%d_%d) '%(x,y)
        # ?
        # stores an array with visible agents in current state
        """self.visibleAgents.append( i )"""

    #
    # Model teammate agents if visible
    #
    # stores their positions in teamPos
    team = self.getTeam( obs )
    #teamPos = []
    for i in team:
      if obs.getAgentPosition(i) is None: 
        continue

      """ myself doesnt count """
      if i == self.index: 
        continue
      
      (x,y) = obs.getAgentPosition(i)
      #teamPos.append( (x,y) )
      
      if obs.getAgentState(i).isPacman is False:
        result += '(friendly_ghost_at p_%d_%d) '%(x,y)
      else:
        result += '(friendly_pacman_at p_%d_%d) '%(x,y)
        
    #
    # Opponent Food. 
    #    
    food = self.getFood( obs ).asList(True)
    for (x,y) in food:
      result += '(enemy_cheese_at p_%d_%d) '%(x,y)
        
    #
    # My Food
    #
    myfood = self.getFoodYouAreDefending(obs).asList(True)
    for (x,y) in myfood:
      result += '(friendly_cheese_at p_%d_%d) '%(x,y)
    
    #
    # Capsule
    #
    capsule = self.getCapsules( obs )
    for (x,y) in capsule:
      result += '(enemy_capsule_at p_%d_%d) '%(x,y)
      
    grid = obs.getWalls()
    dangerPos = []

    for (x, y) in enemyPos:
      dangerPos.append((x,y))  
      if grid[x+1][y] is False:
        dangerPos.append((x+1, y))
      if grid[x][y+1] is False:
        dangerPos.append((x, y+1))
      if grid[x][y-1] is False:
        dangerPos.append((x, y-1))
      if grid[x-1][y] is False:
        dangerPos.append((x-1, y))

    #
    # Describe where the agent can move, and the color of a location
    # Anywhere but walls
    #
    for y in range(grid.height):
      for x in range(grid.width):
        if grid[x][y] is False:
          """ Protected Locations """
          if (x,y) not in dangerPos:
            result += '(is_safe p_%d_%d) '%(x, y)

          """ Location Coloring """
          if x >= grid.width/2 :
            if obs.isOnRedTeam(self.index) is True :
              result += '(is_enemy_side p_%d_%d) '%(x, y)
            else :
              result += '(is_own_side p_%d_%d) '%(x, y)
          else :
            if obs.isOnRedTeam(self.index) is True :
              result += '(is_own_side p_%d_%d) '%(x, y)
            else :
              result += '(is_enemy_side p_%d_%d) '%(x, y)

          """ Reachable Location """
          if grid[x+1][y] is False:
            result += '(can_go p_%d_%d p_%d_%d) '%(x,y,x+1,y)
          if grid[x][y+1] is False:
            result += '(can_go p_%d_%d p_%d_%d) '%(x,y,x,y+1)
          if grid[x-1][y] is False:
            result += '(can_go p_%d_%d p_%d_%d) '%(x,y,x-1,y)
          if grid[x][y-1] is False:
            result += '(can_go p_%d_%d p_%d_%d) '%(x,y,x,y-1)

    return result

  def createPDDLgoal( self ):
    import time
    """
    Return a list of fluents describing the goal states
    This list is used in the PDDL problem file 
    """

    goEat = False
    goKill = True
    goal =''

    #
    # information we need
    #
    obs = self.getCurrentObservation()
    myPos = obs.getAgentPosition( self.index )
    other_team = self.getOpponents( obs )
    team = self.getTeam( obs )
    foodList = self.getFood( obs ).asList(True)

    visibleEnemies = []

    ####################
    # KILL STRATEGY
    # A maybe smarter defensive agent,
    # looks for ghosts that other teammates are not looking at 
    # (i.e. ghosts that are closer to this agent than others)
    ####################
    if goKill is True:

      """ Get teammates position """
      teamPosition = []
      for i in team:
        if obs.getAgentPosition(i) is None: 
          continue
        if i == self.index: 
          continue
        (a,b) = obs.getAgentPosition (i)
        teamPosition.append( (a,b) )

      distances = obs.getAgentDistances()
      closest = (-1,-1)
      closestDistance = 500000

      for enemy in other_team :
        if obs.getAgentPosition(enemy) is None: continue
        if obs.getAgentState(enemy).isPacman is False: continue

        (x,y) = obs.getAgentPosition(i)
        x = int(x)
        y = int(y)
        for teammate in teamPosition:
          d1 = self.getMazeDistance((x,y),teammate)
          d2 = self.getMazeDistance((x,y),myPos)
        if d2 < d1:
          if d2 < closestDistance:
            closestDistance = d2
            closest = (x,y)

      if closest != (-1,-1) :
        goal += '(not (enemy_pacman_at p_%d_%d)) '%(closest[0], closest[1])

    ###################
    # EAT STRATEGY
    # A MAYBE SMARTER OFFENSIVE AGENT
    # looks for food that other teammates are not looking at 
    # (i.e. food that are closer to this agent than others)
    ###################
    teamPos = []
    """ get position of your teammates """
    for i in team:
      """ nothing """
      if obs.getAgentPosition(i) is None: 
        continue
      """ myself doesnt count """
      if i == self.index: 
        continue
      (a,b) = obs.getAgentPosition (i)
      teamPos.append( (int(a),int(b)) )

    closestDistance = 500000
    closest = (-1, -1)
    """ check if food is closer to this agent than other teammates """
    for (x,y) in foodList:
      x = int(x)
      y = int(y)
      for teammate in teamPos:
        """ d1 is the distance btw food and teammate """
        d1 = self.getMazeDistance((x,y),teammate)
        """ d2 is the distance btw food and this agent """
        d2 = self.getMazeDistance((x,y),myPos)
        if d2 < d1:
          """ check if this is the closest food """
          if d2 < closestDistance:
            closestDistance = d2
            closest = (x,y)
    if closest != (-1,-1) :
      goal += '(not (enemy_cheese_at p_%d_%d)) '%(closest[0], closest[1])

    ####################
    # this is needed in case there isnt any goals specified
    # so that an agent would be better to perform some baseline action
    # Here the baseline action is to eat the nearest food to help teammate collect food
    ####################
    # GO EAT
    # A simple offensive agent (whether it is a pacman or not) 
    # just looking for the closest (enemy's) food within its observation
    ####################
    if goal == '':
      goEat = True
    if goEat is True:
      foodList = self.getFood( obs ).asList(True)
      closest = (-1,-1)
      closestDistance = 500000
      for (x,y) in foodList :
        d = self.getMazeDistance(myPos, (int(x),int(y)))
        if d < closestDistance :
          closestDistance = d
          closest = (x,y)
      if closest != (-1,-1) :
        goal += '(not (enemy_cheese_at p_%d_%d)) ' %(closest[0], closest[1])
    return goal

  def generatePDDLproblem(self): 
    """
      outputs a file called problem.pddl describing the initial and
      the goal state
    """

    f = open("./teams/RHJAgents/problem%d.pddl"%self.index,"w");
    lines = list();
    lines.append("(define (problem pacman-problem)\n");
    lines.append("   (:domain pacman-strips)\n");
    lines.append("   (:objects \n");
    lines.append( self.createPDDLobjects() + "\n");
    lines.append(")\n");
    lines.append("   (:init \n");
    lines.append("   ;;primero objetos \n");
    lines.append( self.createPDDLfluents() + "\n");
    lines.append(")\n");
    lines.append("   (:goal \n");          
    lines.append("  ( and  \n");
    lines.append( self.createPDDLgoal() + "\n");
    lines.append("  )\n");
    lines.append("   )\n");
    lines.append(")\n");

    f.writelines(lines);
    f.close();  
        

  def runPlanner( self ):
    """
    runs the planner with the generated problem.pddl. The domain.pddl
    in our case is always the same, as we simply describe once all
    possible actions. The output is redirected into a text file.
    """
    os.system("./ff -o ./teams/RHJAgents/domain.pddl -f ./teams/RHJAgents/problem%d.pddl > ./teams/RHJAgents/solution%d.txt"%(self.index,self.index) );

  def parseSolution( self ):
    """
    Parse the solution file of FF, and selects the first action of the
    plan. Note that you could get  all the actions, not only the first
    one, and call the planner just  when an action in the plan becomes
    unapplicable or the some new  relevant information of the state of
    the game has changed.
    """

    f = open("./teams/RHJAgents/solution%d.txt"%self.index,"r");
    lines = f.readlines();
    f.close();

    for line in lines:
      pos_exec = line.find("0: "); #First action in solution file
      if pos_exec != -1: 
        command = line[pos_exec:];
        command_splitted = command.split(' ')
       
        x = int(command_splitted[3].split('_')[1])
        y = int(command_splitted[3].split('_')[2])

        return (x,y)

      #
      # Empty Plan, Use STOP action, return current Position
      #
      if line.find("ff: goal can be simplified to TRUE. The empty plan solves it") != -1:
        return  self.getCurrentObservation().getAgentPosition( self.index )


  def chooseAction(self, gameState):
    """
    Generate the PDDL problem representing the state of the problem and the goals of the agent.
    Run the planner.
    Parse the solution.
    Send the first action of the plan.
    """
    actions = gameState.getLegalActions(self.index)

    bestAction = 'Stop'
    
    self.generatePDDLproblem()
    self.runPlanner()
    """ if theres no solution found just STOP, do best action otherwise """
    """ STOP to see what the enemies do : ) """
    if self.parseSolution() is None : 
      return bestAction
    else:
      (newx,newy) = self.parseSolution()
      for a in actions:
        succ = self.getSuccessor(gameState, a)
        if succ.getAgentPosition( self.index ) == (newx, newy):
          bestAction = a
          break

    return bestAction
 
  def getSuccessor(self, gameState, action):
    """
    Finds the next successor which is a grid position (location tuple).
    """
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Only half a grid position was covered
      return successor.generateSuccessor(self.index, action)
    else:
      return successor

  def evaluate(self, gameState, action):
    """
    Computes a linear combination of features and feature weights
    """
    features = self.getFeatures(gameState, action)
    weights = self.getWeights(gameState, action)
    return features * weights

  def getFeatures(self, gameState, action):
    """
    Returns a counter of features for the state
    """
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)
    return features

  def getWeights(self, gameState, action):
    """
    Normally, weights do not depend on the gamestate.  They can be either
    a counter or a dictionary.
    """
    return {'successorScore': 1.0}

class OffensiveReflexAgent(ReflexCaptureAgent):
  """
  A reflex agent that seeks food. This is an agent
  we give you to get an idea of what an offensive agent might look like,
  but it is by no means the best or only way to build an offensive agent.
  """
  def getFeatures(self, gameState, action):
    #print "run Offensive"
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)

    # Compute distance to the nearest food
    foodList = self.getFood(successor).asList()
    if len(foodList) > 0: # This should always be True,  but better safe than sorry
      myPos = successor.getAgentState(self.index).getPosition()
      minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
      features['distanceToFood'] = minDistance
      print action, features, successor
    return features

  def getWeights(self, gameState, action):
    return {'successorScore': 100, 'distanceToFood': -1}

class DefensiveReflexAgent(ReflexCaptureAgent):
  """
  A reflex agent that keeps its side Pacman-free. Again,
  this is to give you an idea of what a defensive agent
  could be like.  It is not the best or only way to make
  such an agent.
  """

  def getFeatures(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)

    myState = successor.getAgentState(self.index)
    myPos = myState.getPosition()

    # Computes whether we're on defense (1) or offense (0)
    features['onDefense'] = 1
    if myState.isPacman: features['onDefense'] = 0

    # Computes distance to invaders we can see
    enemies = [successor.getAgentState(i) for i in self.getOpponents(successor)]
    invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
    features['numInvaders'] = len(invaders)
    if len(invaders) > 0:
      dists = [self.getMazeDistance(myPos, a.getPosition()) for a in invaders]
      features['invaderDistance'] = min(dists)

    if action == Directions.STOP: features['stop'] = 1
    rev = Directions.REVERSE[gameState.getAgentState(self.index).configuration.direction]
    if action == rev: features['reverse'] = 1

    return features

  def getWeights(self, gameState, action):
    return {'numInvaders': -1000, 'onDefense': 100, 'invaderDistance': -10, 'stop': -100, 'reverse': -2}


