(define (domain pacman-strips)
  ;(:requirements :typing)
  (:types location)
  ;; "?" denotes a variable, "-" a type
  
  (:predicates

    (is_pacman)
    (is_ghost)

    (at ?xy - location)
    (can_go ?xy ?nxy - location)
    
    (enemy_capsule_at ?xy - location)
    (enemy_cheese_at ?xy - location)
    (enemy_ghost_at ?xy - location)
    (enemy_pacman_at ?xy - location)

    (friendly_capsule_at ?xy - location)
    (friendly_cheese_at ?xy - location)
    (friendly_pacman_at ?xy - location)
    (friendly_ghost_at ?xy - location)

    (is_enemy_side ?xy - location)
    (is_own_side ?xy - location)

    (is_safe ?xy - location)
  )

  ;; Agent pacman moves in enemy area
  (:action Pacman_Move
    :parameters (?xy ?nxy - location)
    
    :precondition (and (at ?xy) 
      (can_go ?xy ?nxy)
      (is_pacman)
      (is_enemy_side ?xy)
      (is_enemy_side ?nxy)
      (is_safe ?nxy)
    )

    :effect (and (at ?nxy)
      (not (at ?xy))
      (not (enemy_cheese_at ?nxy))
      (not (enemy_capsule_at ?nxy)))
  )

  ;; Agent Ghost moves in own area
  (:action Ghost_Move
    :parameters (?xy ?nxy - location)

    :precondition (and
      (is_ghost)
      (can_go ?xy ?nxy)
      (at ?xy)
      (is_own_side ?xy)
      (is_own_side ?nxy)
    )

    :effect (and
      (at ?nxy)
      (not (at ?xy))
      (not (enemy_pacman_at ?nxy))
    )
  )

  ;; Agent ghost moves from own are to enemy area
  (:action Cross_To_Enemy_Side
    :parameters (?xy ?nxy - location)

    :precondition (and
      (is_ghost)
      (can_go ?xy ?nxy)
      (at ?xy)
      (is_own_side ?xy)
      (is_enemy_side ?nxy)
      (is_safe ?nxy)
    )

    :effect (and
      (is_pacman)
      (not (is_ghost))
      (at ?nxy)
      (not (at ?xy))
      (not (enemy_cheese_at ?nxy))
      (not (enemy_capsule_at ?nxy))
    )
  )

  ;; Agent pacman moves from enemy area to own area
  (:action Back_To_Own_Side
    :parameters (?xy ?nxy - location)

    :precondition (and
      (is_pacman)
      (can_go ?xy ?nxy)
      (at ?xy)
      (is_own_side ?nxy)
      (is_enemy_side ?xy)
    )

    :effect (and
      (not (is_pacman))
      (is_ghost)
      (at ?nxy)
      (not (at ?xy))
      (not (enemy_pacman_at ?nxy))
    )
  )
)